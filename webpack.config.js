const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
	mode: "development",
	entry: "./src/index.ts",
	output: {
		filename: "index.js",
		path: path.resolve(__dirname, 'dist')
	},
	devServer: {
		static: {
			directory: path.join(__dirname, 'dist'),
		},
		compress: true,
		port: 9000,
	},
	resolve: {
		extensions: [".webpack.js", ".web.js", ".ts", ".js", 'scss']
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				loader: "ts-loader"
			},
			{
				test: /\.html$/i,
				loader: "html-loader",
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					"style-loader",
					"css-loader",
					"sass-loader",
				],
			},
			{
				test: /\.[tj]s$/,
				enforce: 'pre',
				use: ['source-map-loader'],
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'Shooter 2D'
		}),
	],
}
