export class CountDown {
	constructor(
		readonly maxTime: number,
		public initialTimeLeft: number = 0,
	) {
	}

	tryExecute(action: () => void): void {
		this.update();

		if (this.isActive) {
			action();
			this.reset();
		}
	}

	private update(): void {
		this.initialTimeLeft--;
		this.initialTimeLeft = Math.max(this.initialTimeLeft, 0);
	}

	private get isActive() {
		return this.initialTimeLeft === 0;
	}

	private reset(): void {
		this.initialTimeLeft = this.maxTime;
	}
}