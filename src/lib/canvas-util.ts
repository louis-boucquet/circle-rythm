import { Coordinate } from './coordinate';

export function renderCircle(
	ctx: CanvasRenderingContext2D,
	{
		location: { x, y },
		size,
	}: {
		location: Coordinate,
		size: number,
	},
) {
	ctx.beginPath();
	ctx.arc(x, y, size, 0, 2 * Math.PI);
	ctx.fill();
}

export function renderHealthBar(
	ctx: CanvasRenderingContext2D,
	{
		location,
		size,
		health,
	}: {
		location: Coordinate,
		size: Coordinate,
		health: number,
	},
) {
	location = Coordinate.subtract(
		location,
		Coordinate.scale(size, 0.5),
	);

	ctx.fillStyle = 'red';
	ctx.fillRect(
		location.x,
		location.y,
		size.x,
		size.y,
	);

	ctx.fillStyle = 'green';
	ctx.fillRect(
		location.x,
		location.y,
		size.x * health,
		size.y,
	);
}

export function renderIllumination(
	ctx: CanvasRenderingContext2D,
	{ x, y }: Coordinate,
	innerRadius: number,
	outerRadius: number,
	color: string = 'white'
) {
	const gradient = ctx.createRadialGradient(
		x, y,
		innerRadius,
		x, y,
		outerRadius,
	);

	gradient.addColorStop(0, color);
	gradient.addColorStop(1, 'black');

	ctx.fillStyle = gradient;
	ctx.fillRect(
		x - outerRadius,
		y - outerRadius,
		outerRadius * 2,
		outerRadius * 2,
	);
}
