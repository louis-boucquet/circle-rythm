import { Direction } from './direction';

export type Coordinate = { readonly x: number, readonly y: number };

function clampNumbers(n: number, min: number, max: number) {
	return Math.max(Math.min(n, max), min);
}

export namespace Coordinate {
	export function dotProduct(a: Coordinate, b: Coordinate) {
		return a.x * b.x + a.y * b.y;
	}

	export function add(a: Coordinate, b: Coordinate) {
		return {
			x: a.x + b.x,
			y: a.y + b.y,
		};
	}

	export function subtract(a: Coordinate, b: Coordinate) {
		return {
			x: a.x - b.x,
			y: a.y - b.y,
		};
	}

	export function distance(a: Coordinate, b: Coordinate) {
		const { x, y } = subtract(a, b);
		return Math.sqrt(x * x + y * y);
	}

	export function size({ x, y }: Coordinate) {
		return Math.sqrt(x * x + y * y);
	}

	export function clamp(
		source: Coordinate,
		min: Coordinate,
		max: Coordinate,
	) {
		return {
			x: clampNumbers(source.x, min.x, max.x),
			y: clampNumbers(source.y, min.y, max.y),
		};
	}

	export function clampSize(
		source: Coordinate,
		maxSize: number,
	) {
		return scale(
			normalize(source),
			Math.min(size(source), maxSize),
		);
	}

	export function normalize(
		source: Coordinate,
		requiredSize: number = 1,
	) {
		const size = Coordinate.size(source);
		if (size === 0)
			return { x: 0, y: 0 };

		return scale(source, requiredSize / size);
	}

	export function scale(
		source: Coordinate,
		factor: number,
	) {
		return {
			x: source.x * factor,
			y: source.y * factor,
		};
	}

	export function random({ x, y }: Coordinate) {
		return {
			x: Math.floor(Math.random() * x),
			y: Math.floor(Math.random() * y),
		};
	}

	export function fromDirection(direction: Direction): Coordinate {
		switch (direction) {
			case Direction.LEFT:
				return {x: -1, y: 0}
			case Direction.RIGHT:
				return {x: 1, y: 0}
			case Direction.UP:
				return {x: 0, y: -1}
			case Direction.DOWN:
				return {x: 0, y: 1}
		}
	}
}
