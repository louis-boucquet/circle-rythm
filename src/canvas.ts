import { Coordinate } from './lib/coordinate';

export function getCanvasContext(dimensions: Coordinate) {
	const canvas = document.createElement('canvas');
	canvas.width = dimensions.x;
	canvas.height = dimensions.y;
	const ctx = canvas.getContext('2d');
	if (!ctx)
		throw new Error('No canvas support in browser');
	return ctx;
}

export function getUpScaledCanvas(dimensions: Coordinate, scaleFactor: number) {
	const ctx = getCanvasContext(dimensions);
	ctx.canvas.style.width = `${dimensions.x * scaleFactor}px`;
	ctx.canvas.style.height = `${dimensions.y * scaleFactor}px`;
	return ctx;
}
