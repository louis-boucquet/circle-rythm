import { getCanvasContext } from './canvas';

export class World {
	readonly size = {
		x: window.innerWidth,
		y: window.innerHeight,
	};
	readonly ctx = getCanvasContext(this.size);

	readonly circles = [
		{
			radius: 70,
			color: 'white',
			points: [0, 0.25, 0.5, 0.75],
		},
		{
			radius: 50,
			color: 'red',
			points: [0],
		},
		{
			radius: 30,
			color: 'green',
			points: [0.5],
		},
	];

	private currentAngle = 0;

	constructor() {
		document.body.appendChild(this.ctx.canvas);
	}

	update() {
		this.currentAngle += 0.01;
	}

	render() {
		this.ctx.clearRect(
			0, 0,
			this.size.x,
			this.size.y,
		);

		for (const { radius, points, color } of this.circles) {
			this.ctx.strokeStyle = 'white';
			this.ctx.beginPath();
			this.ctx.arc(this.size.x / 2, this.size.y / 2, radius, 0, 2 * Math.PI);
			this.ctx.stroke();

			for (const position of points) {
				const pointX = Math.cos((position - 0.25) * 2 * Math.PI) * radius + this.size.x / 2;
				const pointY = Math.sin((position - 0.25) * 2 * Math.PI) * radius + this.size.y / 2;

				this.ctx.fillStyle = color;
				this.ctx.beginPath();
				this.ctx.arc(pointX, pointY, 7, 0, 2 * Math.PI);
				this.ctx.fill();
			}
		}

		this.ctx.strokeStyle = 'white';
		this.ctx.beginPath();
		this.ctx.moveTo(this.size.x / 2, this.size.y / 2);
		this.ctx.lineTo(
			Math.cos((this.currentAngle - 0.25) * 2 * Math.PI) * 100 + this.size.x / 2,
			Math.sin((this.currentAngle - 0.25) * 2 * Math.PI) * 100 + this.size.y / 2,
		);
		this.ctx.stroke();
	}
}
