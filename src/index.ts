import './main.scss';
import { World } from './world';

const world = new World(
);

// document.addEventListener('keydown', e => world.activeKeys.add(e.key));
// document.addEventListener('keyup', e => world.activeKeys.delete(e.key));

// world.ctx.canvas.addEventListener('mousedown', () => world.isMouseDown = true);
// world.ctx.canvas.addEventListener('mouseup', () => world.isMouseDown = false);
// world.ctx.canvas.addEventListener('mousemove', e => world.mouseLocation = {
// 	x: e.offsetX / world.scaleFactor,
// 	y: e.offsetY / world.scaleFactor,
// });

renderLoop();

function renderLoop() {
	world.update();
	world.render();

	window.requestAnimationFrame(renderLoop);
}
